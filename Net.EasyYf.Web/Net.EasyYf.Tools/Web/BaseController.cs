﻿using Net.EasyYf.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Unity;


namespace Net.EasyYf.Tools.Web
{
    /// <summary>
    /// 父类控制器
    /// </summary>
    public class BaseController:Controller
    {
        [Dependency]
        public BaseEasyService EasyDb;
        [Dependency]
        public BaseERPSerivce ERPDb;
        [Dependency]
        public BaseSysService SysDb;


        /// <summary>
        /// 当前用户名，字符串
        /// </summary>
        public string CurrentUserName
        {
            get
            {
                return Session["user"] as string;
            }
        }


        /// <summary>
        /// 从http请求中获取当前页，layui table相关
        /// </summary>
        public int Page
        {
            get
            {
                return Convert.ToInt32(Request["page"] ?? "1");
            }
        }

        /// <summary>
        /// 从http请求中分页限制，layui table相关
        /// </summary>
        public int Size
        {
            get
            {
                return Convert.ToInt32(Request["limit"] ?? "10");
            }
        }

    }
}

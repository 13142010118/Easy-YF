namespace Net.EasyYf.SysModel
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using Net.EasyYf.SysModel.Entity;

    public partial class SysDbContext : DbContext
    {
        public SysDbContext()
            : base("name=Sys")
        {
        }

        public virtual DbSet<DSCMA> DSCMA { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

        }
    }
}

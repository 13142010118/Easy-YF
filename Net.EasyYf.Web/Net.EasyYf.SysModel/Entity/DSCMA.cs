namespace Net.EasyYf.SysModel.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("DSCMA")]
    public partial class DSCMA
    {
        [StringLength(10)]
        public string COMPANY { get; set; }

        [StringLength(10)]
        public string CREATOR { get; set; }

        [StringLength(10)]
        public string USR_GROUP { get; set; }

        [StringLength(17)]
        public string CREATE_DATE { get; set; }

        [StringLength(10)]
        public string MODIFIER { get; set; }

        [StringLength(17)]
        public string MODI_DATE { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? FLAG { get; set; }

        [Key]
        [StringLength(10)]
        public string MA001 { get; set; }

        [StringLength(10)]
        public string MA002 { get; set; }

        [StringLength(32)]
        public string MA003 { get; set; }

        [StringLength(255)]
        public string MA004 { get; set; }

        [StringLength(1)]
        public string MA005 { get; set; }

        [StringLength(255)]
        public string MA006 { get; set; }

        [StringLength(20)]
        public string MA007 { get; set; }

        [StringLength(8)]
        public string MA008 { get; set; }

        [StringLength(8)]
        public string MA009 { get; set; }

        [StringLength(255)]
        public string UDF01 { get; set; }

        [StringLength(255)]
        public string UDF02 { get; set; }

        [StringLength(255)]
        public string UDF03 { get; set; }

        [StringLength(255)]
        public string UDF04 { get; set; }

        [StringLength(255)]
        public string UDF05 { get; set; }

        [StringLength(255)]
        public string UDF06 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? UDF51 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? UDF52 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? UDF53 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? UDF54 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? UDF55 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? UDF56 { get; set; }

        [StringLength(255)]
        public string UDF07 { get; set; }

        [StringLength(255)]
        public string UDF08 { get; set; }

        [StringLength(255)]
        public string UDF09 { get; set; }

        [StringLength(255)]
        public string UDF10 { get; set; }

        [StringLength(255)]
        public string UDF11 { get; set; }

        [StringLength(255)]
        public string UDF12 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? UDF57 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? UDF58 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? UDF59 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? UDF60 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? UDF61 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? UDF62 { get; set; }
    }
}

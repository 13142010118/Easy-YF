namespace Net.EasyYf.ERPModel.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("INVMB")]
    public partial class INVMB
    {
        [StringLength(10)]
        public string COMPANY { get; set; }

        [StringLength(10)]
        public string CREATOR { get; set; }

        [StringLength(10)]
        public string USR_GROUP { get; set; }

        [StringLength(17)]
        public string CREATE_DATE { get; set; }

        [StringLength(10)]
        public string MODIFIER { get; set; }

        [StringLength(17)]
        public string MODI_DATE { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? FLAG { get; set; }

        [Key]
        [StringLength(20)]
        public string MB001 { get; set; }

        [StringLength(60)]
        public string MB002 { get; set; }

        [StringLength(60)]
        public string MB003 { get; set; }

        [StringLength(4)]
        public string MB004 { get; set; }

        [StringLength(6)]
        public string MB005 { get; set; }

        [StringLength(6)]
        public string MB006 { get; set; }

        [StringLength(6)]
        public string MB007 { get; set; }

        [StringLength(6)]
        public string MB008 { get; set; }

        [StringLength(255)]
        public string MB009 { get; set; }

        [StringLength(20)]
        public string MB010 { get; set; }

        [StringLength(4)]
        public string MB011 { get; set; }

        [StringLength(10)]
        public string MB012 { get; set; }

        [StringLength(20)]
        public string MB013 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? MB014 { get; set; }

        [StringLength(4)]
        public string MB015 { get; set; }

        [StringLength(4)]
        public string MB016 { get; set; }

        [StringLength(10)]
        public string MB017 { get; set; }

        [StringLength(10)]
        public string MB018 { get; set; }

        [StringLength(1)]
        public string MB019 { get; set; }

        [StringLength(1)]
        public string MB020 { get; set; }

        [StringLength(4)]
        public string MB021 { get; set; }

        [StringLength(1)]
        public string MB022 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? MB023 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? MB024 { get; set; }

        [StringLength(1)]
        public string MB025 { get; set; }

        [StringLength(2)]
        public string MB026 { get; set; }

        [StringLength(1)]
        public string MB027 { get; set; }

        [StringLength(255)]
        public string MB028 { get; set; }

        [StringLength(20)]
        public string MB029 { get; set; }

        [StringLength(8)]
        public string MB030 { get; set; }

        [StringLength(8)]
        public string MB031 { get; set; }

        [StringLength(10)]
        public string MB032 { get; set; }

        [StringLength(1)]
        public string MB033 { get; set; }

        [StringLength(1)]
        public string MB034 { get; set; }

        [StringLength(4)]
        public string MB035 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? MB036 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? MB037 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? MB038 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? MB039 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? MB040 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? MB041 { get; set; }

        [StringLength(1)]
        public string MB042 { get; set; }

        [StringLength(1)]
        public string MB043 { get; set; }

        [StringLength(1)]
        public string MB044 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? MB045 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? MB046 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? MB047 { get; set; }

        [StringLength(4)]
        public string MB048 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? MB049 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? MB050 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? MB051 { get; set; }

        [StringLength(1)]
        public string MB052 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? MB053 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? MB054 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? MB055 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? MB056 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? MB057 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? MB058 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? MB059 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? MB060 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? MB061 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? MB062 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? MB063 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? MB064 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? MB065 { get; set; }

        [StringLength(1)]
        public string MB066 { get; set; }

        [StringLength(10)]
        public string MB067 { get; set; }

        [StringLength(10)]
        public string MB068 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? MB069 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? MB070 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? MB071 { get; set; }

        [StringLength(4)]
        public string MB072 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? MB073 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? MB074 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? MB075 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? MB076 { get; set; }

        [StringLength(6)]
        public string MB077 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? MB078 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? MB079 { get; set; }

        [StringLength(20)]
        public string MB080 { get; set; }

        [StringLength(4)]
        public string MB081 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? MB082 { get; set; }

        [StringLength(1)]
        public string MB083 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? MB084 { get; set; }

        [StringLength(1)]
        public string MB085 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? MB086 { get; set; }

        [StringLength(1)]
        public string MB087 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? MB088 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? MB089 { get; set; }

        [StringLength(4)]
        public string MB090 { get; set; }

        [StringLength(1)]
        public string MB091 { get; set; }

        [StringLength(1)]
        public string MB092 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? MB093 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? MB094 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? MB095 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? MB096 { get; set; }

        [StringLength(1)]
        public string MB100 { get; set; }

        [StringLength(1)]
        public string MB101 { get; set; }

        [StringLength(1)]
        public string MB102 { get; set; }

        [StringLength(1)]
        public string MB103 { get; set; }

        [StringLength(1)]
        public string MB104 { get; set; }

        [StringLength(1)]
        public string MB105 { get; set; }

        [StringLength(1)]
        public string MB106 { get; set; }

        [StringLength(1)]
        public string MB107 { get; set; }

        [StringLength(1)]
        public string MB108 { get; set; }

        [StringLength(1)]
        public string MB109 { get; set; }

        [StringLength(20)]
        public string MB110 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? MB111 { get; set; }

        [StringLength(60)]
        public string MB112 { get; set; }

        [StringLength(1)]
        public string MB113 { get; set; }

        [StringLength(1)]
        public string MB114 { get; set; }

        [StringLength(1)]
        public string MB115 { get; set; }

        [StringLength(1)]
        public string MB116 { get; set; }

        [StringLength(1)]
        public string MB117 { get; set; }

        [StringLength(1)]
        public string MB118 { get; set; }

        [StringLength(1)]
        public string MB119 { get; set; }

        [StringLength(1)]
        public string MB120 { get; set; }

        [StringLength(30)]
        public string MB121 { get; set; }

        [StringLength(30)]
        public string MB122 { get; set; }

        [StringLength(30)]
        public string MB123 { get; set; }

        [StringLength(30)]
        public string MB124 { get; set; }

        [StringLength(60)]
        public string MB125 { get; set; }

        [StringLength(1)]
        public string MB126 { get; set; }

        [StringLength(1)]
        public string MB127 { get; set; }

        [StringLength(60)]
        public string MB128 { get; set; }

        [StringLength(60)]
        public string MB129 { get; set; }

        [StringLength(60)]
        public string MB130 { get; set; }

        [StringLength(20)]
        public string MB131 { get; set; }

        [StringLength(20)]
        public string MB132 { get; set; }

        [StringLength(20)]
        public string MB133 { get; set; }

        [StringLength(255)]
        public string MB134 { get; set; }

        [StringLength(255)]
        public string MB135 { get; set; }

        [StringLength(1)]
        public string MB136 { get; set; }

        [StringLength(1)]
        public string MB137 { get; set; }

        [StringLength(10)]
        public string MB138 { get; set; }

        [StringLength(8)]
        public string MB139 { get; set; }

        [StringLength(1)]
        public string MB140 { get; set; }

        [StringLength(1)]
        public string MB141 { get; set; }

        [StringLength(12)]
        public string MB142 { get; set; }

        [StringLength(10)]
        public string MB143 { get; set; }

        [StringLength(1)]
        public string MB144 { get; set; }

        [StringLength(1)]
        public string MB145 { get; set; }

        [StringLength(10)]
        public string MB146 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? MB147 { get; set; }

        [StringLength(4)]
        public string MB148 { get; set; }

        [StringLength(4)]
        public string MB149 { get; set; }

        [StringLength(10)]
        public string MB150 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? MB151 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? MB152 { get; set; }

        [StringLength(10)]
        public string MB179 { get; set; }

        [StringLength(60)]
        public string MB180 { get; set; }

        [StringLength(60)]
        public string MB181 { get; set; }

        [StringLength(60)]
        public string MB182 { get; set; }

        [StringLength(60)]
        public string MB183 { get; set; }

        [StringLength(60)]
        public string MB184 { get; set; }

        [StringLength(60)]
        public string MB185 { get; set; }

        [StringLength(60)]
        public string MB186 { get; set; }

        [StringLength(60)]
        public string MB187 { get; set; }

        [StringLength(60)]
        public string MB188 { get; set; }

        [StringLength(60)]
        public string MB189 { get; set; }

        [StringLength(60)]
        public string MB190 { get; set; }

        [StringLength(60)]
        public string MB191 { get; set; }

        [StringLength(60)]
        public string MB192 { get; set; }

        [StringLength(60)]
        public string MB193 { get; set; }

        [StringLength(60)]
        public string MB194 { get; set; }

        [StringLength(60)]
        public string MB195 { get; set; }

        [StringLength(60)]
        public string MB196 { get; set; }

        [StringLength(60)]
        public string MB197 { get; set; }

        [StringLength(60)]
        public string MB198 { get; set; }

        [StringLength(60)]
        public string MB199 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? MB401 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? MB402 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? MB403 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? MB404 { get; set; }

        [StringLength(1)]
        public string MB405 { get; set; }

        [StringLength(3)]
        public string MB406 { get; set; }

        [StringLength(1)]
        public string MB407 { get; set; }

        [StringLength(4)]
        public string MB408 { get; set; }

        [StringLength(10)]
        public string MB409 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? MB410 { get; set; }

        [StringLength(1)]
        public string MB411 { get; set; }

        [StringLength(10)]
        public string MB412 { get; set; }

        [StringLength(1)]
        public string MB413 { get; set; }

        [StringLength(10)]
        public string MB414 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? MB415 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? MB416 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? MB417 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? MB418 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? MB419 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? MB420 { get; set; }

        [StringLength(1)]
        public string MB421 { get; set; }

        [StringLength(10)]
        public string MB422 { get; set; }

        [StringLength(1)]
        public string MB423 { get; set; }

        [StringLength(10)]
        public string MB424 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? MB425 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? MB426 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? MB427 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? MB428 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? MB429 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? MB430 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? MB431 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? MB432 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? MB433 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? MB434 { get; set; }

        [StringLength(1)]
        public string MB435 { get; set; }

        [StringLength(8)]
        public string MB436 { get; set; }

        [StringLength(30)]
        public string MB437 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? MB438 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? MB439 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? MB440 { get; set; }

        [StringLength(1)]
        public string MB441 { get; set; }

        [StringLength(1)]
        public string MB442 { get; set; }

        [StringLength(1)]
        public string MB443 { get; set; }

        [StringLength(4)]
        public string MB444 { get; set; }

        [StringLength(6)]
        public string MB445 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? MB446 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? MB447 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? MB448 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? MB449 { get; set; }

        [StringLength(1)]
        public string MB450 { get; set; }

        [StringLength(8)]
        public string MB451 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? MB452 { get; set; }

        [StringLength(1)]
        public string MBD01 { get; set; }

        [StringLength(1)]
        public string MBD02 { get; set; }

        [StringLength(10)]
        public string MBE01 { get; set; }

        [StringLength(30)]
        public string MB453 { get; set; }

        [StringLength(1)]
        public string MB454 { get; set; }

        [StringLength(1)]
        public string MBH01 { get; set; }

        [StringLength(10)]
        public string MBH02 { get; set; }

        [StringLength(10)]
        public string MBH03 { get; set; }

        [StringLength(1)]
        public string MB455 { get; set; }

        [StringLength(6)]
        public string MB456 { get; set; }

        [StringLength(6)]
        public string MB457 { get; set; }

        [StringLength(6)]
        public string MB458 { get; set; }

        [StringLength(6)]
        public string MB459 { get; set; }

        [StringLength(6)]
        public string MB460 { get; set; }

        [StringLength(6)]
        public string MB461 { get; set; }

        [StringLength(6)]
        public string MB462 { get; set; }

        [StringLength(6)]
        public string MB463 { get; set; }

        [StringLength(6)]
        public string MB464 { get; set; }

        [StringLength(6)]
        public string MB465 { get; set; }

        [StringLength(6)]
        public string MB466 { get; set; }

        [StringLength(6)]
        public string MB467 { get; set; }

        [StringLength(6)]
        public string MB468 { get; set; }

        [StringLength(6)]
        public string MB469 { get; set; }

        [StringLength(6)]
        public string MB470 { get; set; }

        [StringLength(6)]
        public string MB471 { get; set; }

        [StringLength(6)]
        public string MB472 { get; set; }

        [StringLength(6)]
        public string MB473 { get; set; }

        [StringLength(6)]
        public string MB474 { get; set; }

        [StringLength(6)]
        public string MB475 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? MBL01 { get; set; }

        [StringLength(1)]
        public string MBL02 { get; set; }

        [StringLength(1)]
        public string MBL03 { get; set; }

        [StringLength(1)]
        public string MBL04 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? MB476 { get; set; }

        [StringLength(1)]
        public string MB477 { get; set; }

        [StringLength(1)]
        public string MB478 { get; set; }

        [StringLength(1)]
        public string MB479 { get; set; }

        [StringLength(10)]
        public string MB480 { get; set; }

        [StringLength(255)]
        public string UDF01 { get; set; }

        [StringLength(255)]
        public string UDF02 { get; set; }

        [StringLength(255)]
        public string UDF03 { get; set; }

        [StringLength(255)]
        public string UDF04 { get; set; }

        [StringLength(255)]
        public string UDF05 { get; set; }

        [StringLength(255)]
        public string UDF06 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? UDF51 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? UDF52 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? UDF53 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? UDF54 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? UDF55 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? UDF56 { get; set; }

        [StringLength(255)]
        public string UDF07 { get; set; }

        [StringLength(255)]
        public string UDF08 { get; set; }

        [StringLength(255)]
        public string UDF09 { get; set; }

        [StringLength(255)]
        public string UDF10 { get; set; }

        [StringLength(255)]
        public string UDF11 { get; set; }

        [StringLength(255)]
        public string UDF12 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? UDF57 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? UDF58 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? UDF59 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? UDF60 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? UDF61 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? UDF62 { get; set; }
    }
}

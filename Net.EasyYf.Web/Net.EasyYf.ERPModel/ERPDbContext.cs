namespace Net.EasyYf.ERPModel
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using Net.EasyYf.ERPModel.Entity;

    public partial class ERPDbContext : DbContext
    {
        public ERPDbContext()
            : base("name=ERP")
        {
        }

        public virtual DbSet<INVMB> INVMB { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }
    }
}

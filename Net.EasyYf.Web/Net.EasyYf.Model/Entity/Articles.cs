namespace Net.EasyYf.Model.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Articles
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public string Memo { get; set; }

        public DateTime CreateTime { get; set; }

        public string Editor { get; set; }

        public int TypeID { get; set; }

        public string TypeName { get; set; }

        public string Pic { get; set; }

        public int IsPublished { get; set; }

        public string Author { get; set; }

        public int VersionId { get; set; }

        public int OrderId { get; set; }

        public string DictTypeName { get; set; }
    }
}

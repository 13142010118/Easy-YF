namespace Net.EasyYf.Model
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using Net.EasyYf.Model.Entity;

    public partial class EasyDbContext : DbContext
    {
        public EasyDbContext()
            : base("name=Easy")
        {
        }

        public virtual DbSet<Articles> Articles { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }
    }
}

namespace Net.EasyYf.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _20190817001 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Articles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        Memo = c.String(),
                        CreateTime = c.DateTime(nullable: false),
                        Editor = c.String(),
                        TypeID = c.Int(nullable: false),
                        TypeName = c.String(),
                        Pic = c.String(),
                        IsPublished = c.Int(nullable: false),
                        Author = c.String(),
                        VersionId = c.Int(nullable: false),
                        OrderId = c.Int(nullable: false),
                        DictTypeName = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Articles");
        }
    }
}

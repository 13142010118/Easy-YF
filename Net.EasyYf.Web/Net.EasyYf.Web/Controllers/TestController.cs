﻿using Net.EasyYf.Services;
using Net.EasyYf.SysModel.Entity;
using Net.EasyYf.Utils.Common;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Unity;

namespace Net.EasyYf.Web.Controllers
{
    public class TestController : Controller
    {
        protected Logger logger = LogManager.GetCurrentClassLogger();

        [Dependency]
        public BaseEasyService EasyDb;
        [Dependency]
        public BaseERPSerivce ERPDb;
        [Dependency]
        public BaseSysService SysDb;
        // GET: Test
        public ActionResult Index()
        {
            var list = EasyDb.Db.Articles.Where(o => o.Id > 0).ToList();


            return Json(list,JsonRequestBehavior.AllowGet);
        }

        public ActionResult ERP()
        {
            var list = ERPDb.Db.INVMB.OrderByDescending(O=>O.MB001).Take(10).ToList();


            return Json(list, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Sys()
        {
            logger.Info("访问一个方法");
            var list = SysDb.Db.DSCMA.OrderByDescending(o=>o.MA001).Take(10).ToList();
            var user = SysDb.Find<DSCMA>(o => o.MA001 == "song");
            if (user != null)
            {
                string pwd = YfUserTool.GetUserCode("song", "tomato123");
                if (user.MA003 == pwd)
                {
                    return Content("OK");
                }
            }
            return Json(list, JsonRequestBehavior.AllowGet);
        }
    }
}